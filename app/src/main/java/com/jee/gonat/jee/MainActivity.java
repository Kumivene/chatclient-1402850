package com.jee.gonat.jee;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

import java.util.logging.LogRecord;

import static com.jee.gonat.jee.R.id.printti;
import static com.jee.gonat.jee.R.id.testbutton;

public class MainActivity extends AppCompatActivity {

    private Handler uiHandler	=	new	Handler()	{

        public void handleMessage(Message msg)	{
            if	(msg.what	==	0){
                TextView result	=	(TextView)findViewById(R.id.printti);
                result.append((String) msg.obj + System.getProperty ("line.separator"));
            }
        }
    };
    @Override




    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        setContentView(R.layout.activity_main);
        Button btn	=	(Button)findViewById(R.id.testbutton);

        final TextView kirjotusIkkuna = (TextView)findViewById(R.id.testedit);

        //nappula toiminnot
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String testiteksti = kirjotusIkkuna.getText().toString(); //ottaa teksteditistä senhetkisen tekstin
                Log.e("Puhelin", testiteksti);//debug printti
                String adressi = testiteksti;
                Intent myIntent = new Intent(MainActivity.this, ChatClient.class);
                myIntent.putExtra("key", adressi); //Optional parameters
                MainActivity.this.startActivity(myIntent);

                kirjotusIkkuna.setText("");// tyhjennetään edittext

            }
        });


            }


            public boolean onCreateOptionsMenu(Menu menu) {
                // Inflate the menu; this adds items to the action bar if it is present.
                getMenuInflater().inflate(R.menu.menu_main, menu);
                return true;
            }


        }
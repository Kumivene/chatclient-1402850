package com.jee.gonat.jee;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Socket;



public class Reader implements Runnable{

    private Handler handler;
    private InetSocketAddress osoite;
    private String teksti;
    private BufferedReader in;
    private Socket soketti;

    public Reader(Handler handleri, Socket s,InetSocketAddress a){
        this.soketti = s;
        this.handler = handleri;
        this.osoite = a;
    }
    public void run(){
        //start thread and connect socket
        try {
            soketti.connect(osoite);
            Log.e("readerista", "soketti koneektannut");

            in = new BufferedReader(new InputStreamReader(soketti.getInputStream()));
            Log.e("readerista","bufferille annettu osoite");

        } catch (IOException e) {

            e.printStackTrace();
            Log.e("readerista", "soketti ei konnektannut" + e.toString());
        }

        //while loop for reading messages
        while(true){
            try {
                Log.e("readerin whilesta","ennen readlinea");

                String str="";
                String inStr = in.readLine() + "\r";//save input to a variable
                Log.e("ennen whilea",inStr);

                while	(inStr !=	null){//weird while loop from petri example
                    Log.e("ihme whilen sisällä",inStr);
                    Message	msg	=	handler.obtainMessage();
                    msg.obj	= inStr;
                    msg.what = 0;
                    if(!inStr.equals("")) {
                        handler.sendMessage(msg);
                    }
                    inStr = in.readLine();
                }
                in.close();

                Log.e("handlerin jalkeen",str);

            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.e("readerin hilesta", teksti);
        }

    }

}

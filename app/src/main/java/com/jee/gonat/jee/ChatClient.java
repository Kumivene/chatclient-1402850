package com.jee.gonat.jee;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;

import java.net.InetSocketAddress;
import java.net.Socket;


public class ChatClient extends Activity {

    private Handler uiHandler	=	new	Handler()	{

        public void handleMessage(Message msg)	{
            if	(msg.what	==	0){
                TextView result	=	(TextView)findViewById(R.id.printti2);
                result.append((String) msg.obj + System.getProperty ("line.separator"));
            }
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        final InetSocketAddress adressi = new InetSocketAddress(intent.getStringExtra("key"), 100);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        setContentView(R.layout.activity_chat_client);
        Button btn	=	(Button)findViewById(R.id.testbutton2);

        final TextView kirjotusIkkuna = (TextView)findViewById(R.id.testedit2);
        final TextView printtiIkkuna = (TextView)findViewById(R.id.printti2);

        final ScrollView sv = (ScrollView)findViewById(R.id.scrolli2);

        final Socket soketti = new Socket();
        Reader lukija = new Reader(uiHandler,soketti,adressi);
        Thread t1 = new Thread(lukija);
        t1.start();


        //nappula toiminnot
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String testiteksti = kirjotusIkkuna.getText().toString(); //ottaa teksteditistä senhetkisen tekstin
                Log.e("Puhelin",testiteksti);//debug printti
                Writer wr = new Writer(soketti, testiteksti);//tehdään uusi wirter ja annetaan sille konstruktoriin vietiteksti
                Thread wThread = new Thread(wr);//tehdään thredi joka kuolee heti kun viesti on lähetetty
                wThread.start();

                //scrollausta varten
                /*sv.postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        sv.fullScroll(View.FOCUS_DOWN);
                    }
                }, 100);*/

                kirjotusIkkuna.setText("");// tyhjennetään edittext


            }
        });




    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}